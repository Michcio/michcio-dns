from setuptools import setup

setup(
    name="octodns_fastmail",
    version="0.1.0",
    install_requires=["octodns_merge", "octodns>=0.9.17"],
    packages=["octodns_fastmail"],
    python_requires=">=3.9",
)
