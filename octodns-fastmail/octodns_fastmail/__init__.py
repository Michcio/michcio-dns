import logging
from octodns.record import Record
from octodns.source.base import BaseSource
from octodns_merge import merge_record

class FastmailSource(BaseSource):
    SUPPORTS_GEO = False
    SUPPORTS = {"CNAME", "TXT", "MX"}

    DEFAULT_TTL = 3600

    def __init__(self, id, ttl=DEFAULT_TTL, wildcard=False):
        klass = self.__class__.__name__
        self.log = logging.getLogger(f'{klass}[{id}]')
        super().__init__(id)
        self.ttl = ttl
        self.wildcard = wildcard

    def _dkim_payloads(self, zone_name: str):
        key_names = {"mesmtp", "fm1", "fm2", "fm3"}
        return {
            f"{key_name}._domainkey": {
                "ttl": self.ttl,
                "type": "CNAME",
                "value": f"{key_name}.{zone_name.rstrip('.')}.dkim.fmhosted.com.",
            }
            for key_name in key_names
        }

    def populate(self, zone, target=False, lenient=False):
        mx_payload = {
            "ttl": self.ttl,
            "type": "MX",
            "values": [
                {
                    "exchange": "in1-smtp.messagingengine.com.",
                    "preference": 10,
                },
                {
                    "exchange": "in2-smtp.messagingengine.com.",
                    "preference": 20,
                },
            ],
        }
        spf_payload = {
            "ttl": self.ttl,
            "type": "TXT",
            "values": ["v=spf1 include:spf.messagingengine.com ~all"],
        }
        zone.add_record(Record.new(zone, "", mx_payload, source=self, lenient=lenient), lenient=lenient)
        if self.wildcard:
            zone.add_record(Record.new(zone, "*", mx_payload, source=self, lenient=lenient), lenient=lenient)
        merge_record(zone, Record.new(zone, "", spf_payload, source=self, lenient=lenient))
        for name, payload in self._dkim_payloads(zone.name).items():
            zone.add_record(Record.new(zone, name, payload, source=self, lenient=lenient), lenient=lenient)
