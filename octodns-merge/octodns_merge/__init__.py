from octodns.record import Record
from octodns.zone import DuplicateRecordException, Zone

def merge_record(zone: Zone, record: Record):
    try:
        zone.add_record(record)
    except DuplicateRecordException:
        assert record._type in {"TXT"}
        old = next(old for old in zone.records if record == old)
        record.ttl = min(record.ttl, old.ttl)
        record.values = old.values + record.values
        zone.add_record(record, replace=True)
