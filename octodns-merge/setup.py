from setuptools import setup

setup(
    name="octodns_merge",
    version="0.1.0",
    install_requires=["octodns>=0.9.17"],
    packages=["octodns_merge"],
    python_requires=">=3.9",
)
